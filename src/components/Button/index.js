import React from 'react';
import {
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';

function Button({children, style, ...rest}) {
  return Platform.OS === 'ios' ? (
    <TouchableOpacity {...rest}>{children}</TouchableOpacity>
  ) : (
    <TouchableNativeFeedback {...rest}>
      <View style={style}>{children}</View>
    </TouchableNativeFeedback>
  );
}

export default Button;
