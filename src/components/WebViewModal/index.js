import React, {forwardRef, useRef, useState} from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import {WebView} from 'react-native-webview';
import {Spinner} from '..';
import styles from './style';

function WebViewModal({visible, uri, onClose,btnOnTop}) {

  function renderLoading() {
    return <Spinner />;
  }

  return (
    <Modal
      isVisible={visible}
      useNativeDriver
      animationIn="pulse"
      animationInTiming={400}
      onBackdropPress={onClose}
      onBackButtonPress={onClose}
      style={styles.viewModal}
      >
      <View style={styles.content}>
        {btnOnTop&& btnOnTop()}
        <WebView
          source={{uri}}
          startInLoadingState
          style={styles.webViewStyle}
          renderLoading={renderLoading}
        />
      </View>
    </Modal>
  );
}

export default WebViewModal;
