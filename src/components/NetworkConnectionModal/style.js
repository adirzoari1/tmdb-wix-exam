import {StyleSheet} from 'react-native';
import {windowHeight, windowWidth} from '../../utils/helpers';
import * as theme from '../../utils/theme';
const style = StyleSheet.create({
  viewModal:{
    justifyContent: 'flex-end',
    margin: 0,
  },
  content:{
    backgroundColor: theme.colors.white,
    padding: 20,
    borderTopRightRadius:10,
    borderTopLeftRadius:10,
    height:300,
    justifyContent:'center',
    alignItems:'center'
  },
  viewEmoji:{
    fontSize: 60,
  },
  textHeader:{
    fontSize:20,
    fontFamily:theme.fonts.bold,
    color:theme.colors.purple
  },
  textSubHeader:{
    fontSize:13,
    fontFamily:theme.fonts.medium,
    color:theme.colors.black,
    marginVertical:2
  }
});

export default style;
