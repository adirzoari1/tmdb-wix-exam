import React, {useState, useMemo, useCallback} from 'react';
import {View, Text, FlatList} from 'react-native';
import styles from './style';
import {useSelector, useDispatch} from 'react-redux';
import {moviesWatchlistIdsSelector} from '../../store/watchlist/watchlist.selectors';
import {selectedWatchMovieInfoSelector} from '../../store/movie/movie.selectors';
import Icon from 'react-native-vector-icons/Ionicons';
import * as theme from '../../utils/theme';
import {
  addMovieToWatchList,
  removeMovieFromWatchList,
} from '../../store/watchlist/watchlist.actions';
import {MovieItem, WebViewModal, Button} from '..';
import {getYoutubeVideoURL} from '../../utils/helpers';
import {getMovieInfoRequest} from '../../store/movie/movie.actions';
import Share from 'react-native-share';

function MoviesList({
  data,
  refreshing,
  handleRefresh,
  handleLoadMore,
  headerListTitle,
  listFooterComponent,
  listEmptyComponent,
}) {
  const dispatch = useDispatch();
  const moviesWatchlistIds = useSelector(moviesWatchlistIdsSelector);
  const selectedWatchMovieInfo = useSelector(selectedWatchMovieInfoSelector);
  const [showWatchMovieModal, setShowWatchMovieModal] = useState(false);

  const movieURL = useMemo(() => {
    return getYoutubeVideoURL(selectedWatchMovieInfo);
  }, [selectedWatchMovieInfo]);

  function handlePressFav(movie) {
    if (moviesWatchlistIds[movie.id])
      dispatch(removeMovieFromWatchList(movie.id));
    else dispatch(addMovieToWatchList(movie));
  }

  function handleWatchVideo(movie) {
    dispatch(getMovieInfoRequest(movie.id));
    setShowWatchMovieModal(true);
  }
  
  function closeWatchMovieModal() {
    setShowWatchMovieModal(false);
  }


  async function shareMovieButton() {
    try {
      await Share.open({
        title: 'Share a movie',
        message: `Hi 👋, I want to share with you the movie ${selectedWatchMovieInfo.title}`,
        url: movieURL,
      });
    } catch (e) {}
  }

  

  const renderListHeader = useCallback(() =>{
    return(
      data?.length?<View>
      <Text style={styles.textHeaderList}>{headerListTitle}</Text>
    </View>:null
    )
  } ,[data])

  const renderWebViewButton = useCallback(() => {
    return (
      <Button onPress={shareMovieButton} style={styles.viewShareMovieButton}>
      <View style={styles.viewShareIcon}>
        <Icon
          name="share-social-outline"
          size={30}
          color={theme.colors.white}
        />
      </View>
    </Button>
    )
  },[selectedWatchMovieInfo])


  const renderMovieItem = useCallback(({item}) => {
    return (
      <MovieItem
      key={item.id}
      isFavorite={moviesWatchlistIds[item.id]}
      item={item}
      onPressFav={handlePressFav}
      onPressWatchVideo={handleWatchVideo}/>
    )}
  ,[data,moviesWatchlistIds])
    
  const keyExtractor = useCallback((item)=>{
    return item.id.toString()
  },[])

  return (
    <View style={styles.viewMoviesList}>
      <WebViewModal
        visible={showWatchMovieModal}
        uri={movieURL}
        onClose={closeWatchMovieModal}
        btnOnTop={renderWebViewButton}/>

      <FlatList
        data={data}
        extraData={moviesWatchlistIds}
        keyExtractor={keyExtractor}
        initialNumToRender={6}
        windowSize={6}
        style={styles.flatList}
        renderItem={renderMovieItem}
        refreshing={refreshing}
        onRefresh={handleRefresh}
        ListHeaderComponent={renderListHeader}
        ListEmptyComponent={listEmptyComponent}
        ListFooterComponent={listFooterComponent}
        contentContainerStyle={styles.contentFlatList}
        onEndReachedThreshold={0.1}
        maxToRenderPerBatch={5}
      />
    </View>
  );
}

export default MoviesList;

MoviesList.defaultProps = {
  data: [],
  refreshing: false,
  handleRefresh: () => {},
  handleLoadMore: () => {},
  headerListTitle: 'null',
  listFooterComponent: null,
  listEmptyComponent: null,
};
