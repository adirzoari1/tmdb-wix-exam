import {StyleSheet} from 'react-native';
import * as theme from '../utils/theme';

const styles = StyleSheet.create({
  tabStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 4,
    backgroundColor: theme.colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  tabBarStyle: {
    borderTopWidth: 0,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    position: 'absolute',
    backgroundColor: theme.colors.white,
    shadowColor: theme.colors.black_o20,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowRadius: 30,
    elevation: 5,
    shadowOpacity: 1,
    height: 55,
  },
  tabBarBadgeStyle: {
    backgroundColor: theme.colors.yellow,
  },
  containerApp:{
    flex:1
  }
});

export default styles;
