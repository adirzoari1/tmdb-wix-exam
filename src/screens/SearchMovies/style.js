import {StyleSheet} from 'react-native';
import * as theme from '../../utils/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.black,
   
  },
  viewSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: theme.colors.white,
    paddingHorizontal: 25,
    height: 60,
    width: '100%',
  },
  viewSearchInput: {
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewSpinner: {
    justifyContent: 'center',
    alignItems: 'center',
    height:'100%',
  },
  styleText:{
    fontSize:14,
    fontFamily:theme.fonts.light,
    width:'90%'
  },
  viewEmptyList: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  textEmptyList: {
    fontFamily: theme.fonts.regular,
    color: theme.colors.white,
    fontSize: 18,
  },
  subTextEmptyList: {
    fontFamily: theme.fonts.grey,
    color: theme.colors.white,
    fontSize: 12,
    width: '60%',
  },
  contentFlatList: {
    flexGrow: 1,
  },
});

export default styles;
