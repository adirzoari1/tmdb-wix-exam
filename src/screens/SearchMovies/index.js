import React, {useCallback, useEffect, useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './style';
import {Spinner, MoviesList, SearchBar} from '../../components';
import * as theme from '../../utils/theme';
import {useSelector, useDispatch} from 'react-redux';
import {
  isLoadingSelector,
  searchMoviesResponseSelector,
} from '../../store/movie/movie.selectors';
import {
  resetSearch,
  searchMoviesRequest,
} from '../../store/movie/movie.actions';
import useDebounce from '../../utils/hooks/useDebounce';
import { isConnectedSelector } from '../../store/app/app.selectors';

function SearchMovies() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const searchMoviesResponse = useSelector(searchMoviesResponseSelector);
  const isConnected = useSelector(isConnectedSelector);

  const isLoading = useSelector(isLoadingSelector);
  const [searchTerm, setSearchTerm] = useState('');
  const onSearchMovie = useDebounce(searchMovie, 250);

  useEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () => {
      parent.setOptions({
        tabBarVisible: true,
      });
      dispatch(resetSearch());
    };
  }, []);

  useEffect(() => {
    if(isConnected)
      onSearchMovie();
  }, [searchTerm]);


  function onPressBack() {
    navigation.goBack();
  }

  function onSubmitEditing(){
    searchMovie()
  }
  function onChangeText(value) {
    setSearchTerm(value.trim());
  }

  function searchMovie() {
    if (searchTerm) {
      dispatch(searchMoviesRequest({query: searchTerm}));
    } else {
      dispatch(resetSearch());
    }
  }

  const renderEmptyList = useCallback(()=> {
    return (
      <View style={styles.viewEmptyList}>
        <Icon name="file-tray-outline" size={40} color={theme.colors.purple} />
        <Text numberOfLines={2} style={styles.textEmptyList}>
          No found movies for
        </Text>
        <Text style={styles.subTextEmptyList}>{searchTerm}</Text>
      </View>
    );
  },[searchTerm])
  return (
    <SafeAreaView style={styles.container}>
      <SearchBar
        value={searchTerm}
        colorIconSearch={theme.colors.black}
        colorIconSearch={theme.colors.black}
        onChangeText={onChangeText}
        placeHolderText="Search movies..."
        placeHolderTextColor={theme.colors.grey_a}
        onPressCloseIcon={onPressBack}
        onSubmitEditing={onSubmitEditing}
        styleViewSearch={styles.viewSearch}
        styleViewSearchInput={styles.viewSearchInput}
        styleText={styles.styleText}
      />
      {isLoading && (
        <View style={styles.viewSpinner}>
          <Spinner />
        </View>
      )}
      <MoviesList
        data={searchMoviesResponse.results}
        listEmptyComponent={searchTerm && !isLoading && renderEmptyList}
        headerListTitle={`Results (${searchMoviesResponse.results?.length})`}
        listFooterComponent={searchTerm && renderEmptyList}
      />
    </SafeAreaView>
  );
}

export default SearchMovies;
