import React, {useCallback, useEffect, useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {Spinner, MoviesList, Button} from '../../components';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
import * as theme from '../../utils/theme';
import {
  moviesResponseSelector,
  isLoadingSelector,
} from '../../store/movie/movie.selectors';
import {getPopularMoviesRequest} from '../../store/movie/movie.actions';
import {
  isConnectedSelector,
} from '../../store/app/app.selectors';

function Movies() {
  const navigation = useNavigation();
  const isLoading = useSelector(isLoadingSelector);
  const isConnected = useSelector(isConnectedSelector);
  const dispatch = useDispatch();
  const moviesResponse = useSelector(moviesResponseSelector);
  const [page, setPage] = useState(1);
  const [refreshing, setRefresing] = useState(false);


  useEffect(()=>{
    // reset when network changes state to connected
    if(isConnected){
      setPage(1)

    }
  },[isConnected])
  useEffect(() => {
    if (isConnected && moviesResponse.totalPages != page) {
      getPopularMovies({page});
    }
  }, [page]);

  useEffect(() => {
    if (isLoading) {
      setRefresing(false);
    }
  }, [isLoading]);

  function getPopularMovies(data) {
    dispatch(getPopularMoviesRequest(data));
  }

  function handleRefresh() {
    if (page != 1) {
      setPage(1);
      setRefresing(true);
    }
  }

  function handleLoadMoreMovies() {
    setPage(page => page + 1);
  }

  function handlePressSearchInput() {
    navigation.navigate('SearchMovies');
  }

  const renderListFooter = useCallback(() => {
    if (isLoading)
      return (
        <View style={styles.viewSpinner}>
          <Spinner />
        </View>
      );

    if (
      moviesResponse.page !== moviesResponse.totalPages &&
      moviesResponse.results
    ) {
      return (
        <View style={styles.viewLoadMoreMovies}>
          <Button
            style={styles.btnShowMoreMovies}
            onPress={handleLoadMoreMovies}>
            <Text style={styles.textShowMoreMovies}> Load more </Text>
          </Button>
        </View>
      );
    }
    if (moviesResponse.results.length)
      return <View style={styles.viewLoadMoreMovies} />;

    return null;
  },[moviesResponse,page,isLoading])
  
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.viewHeader}>
        <Text style={styles.textHeader}>Hey 🖐</Text>
        <Text style={styles.textSubHeader}>
          Let's find your <Text style={styles.textHeaderMovie}>movie</Text>{' '}
        </Text>
      </View>

      <View style={styles.viewSearchFilters}>
        <Button
          style={styles.viewButtonSearchInput}
          onPress={handlePressSearchInput}>
          <Icon name="search" color={theme.colors.black} size={20} />
          <Text style={styles.textSearchMovies}> Search Movies </Text>
        </Button>
      </View>

      <MoviesList
        data={moviesResponse.results}
        headerListTitle={`Popular Movies (${moviesResponse.results?.length})`}
        listFooterComponent={isConnected && renderListFooter}
        refreshing={refreshing}
        handleRefresh={handleRefresh}
      />
    </SafeAreaView>
  );
}

export default Movies;
