import {StyleSheet} from 'react-native';
import { windowWidth } from '../../utils/helpers';
import * as theme from '../../utils/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.black,
  },
  viewHeader: {
    marginTop: 40,
    paddingHorizontal: 20,
  },
  textHeader:{
    fontFamily:theme.fonts.bold,
    fontSize:25,
    color: theme.colors.white,
    marginVertical:20,
  },
  textSubHeader: {
    fontFamily:theme.fonts.regular,
    fontSize:20,
    color: theme.colors.white,
  },
  textHeaderMovie:{
    fontFamily:theme.fonts.regular,
    fontSize:20,
    color:theme.colors.purple
  },

  viewLoadMoreMovies:{
    paddingVertical: 25,
    paddingBottom:40,
    justifyContent:'center',
    alignItems:'center',
  },
  btnShowMoreMovies:{
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:theme.colors.purple,
     borderWidth:1,
     padding:10,
     borderRadius:20
  },
  textShowMoreMovies:{
    fontFamily:theme.fonts.light,
    fontSize:15,
    color:theme.colors.white
  },
  viewSearchFilters:{
    flexDirection:'row',
    alignItems:'center',
  },
  viewButtonSearchInput:{
    height:50,
    width:windowWidth-50,
    marginVertical:20,
    marginHorizontal:20,
    backgroundColor:theme.colors.grey,
    borderRadius:20,
    flexDirection:'row',
    alignItems:'center',
    paddingLeft:25,
  },
  textSearchMovies:{
    fontFamily:theme.fonts.light,
    fontSize:15,
    color:theme.colors.black
  },
  viewSpinner:{
    marginTop:10,
    paddingBottom:50,
  }
});

export default styles;
