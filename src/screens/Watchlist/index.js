import React from 'react';
import {SafeAreaView, View, Text} from 'react-native';
import {useSelector} from 'react-redux';
import {MoviesList} from '../../components';
import styles from './style';
import {moviesWatchlistSelector} from '../../store/watchlist/watchlist.selectors';

function WatchList() {
  const moviesWatchlist = useSelector(moviesWatchlistSelector);

  const renderEmptyList = () => (
    <View style={styles.viewEmptyWatchlist}>
        <Text style={styles.textEmoji}>😏</Text>
      <Text style={styles.textEmptyWatchlist}>Watchlist is empty...</Text>
      <Text style={styles.subTextEmptyWatchlist}>
        Time to add your first movie 🍿
      </Text>
    </View>
  );



  return (
    <SafeAreaView style={styles.container}>
      <MoviesList
        data={moviesWatchlist}
        headerListTitle={`My Watchlist (${moviesWatchlist.length})`}
        listEmptyComponent={renderEmptyList}
      />
    </SafeAreaView>
  );
}

export default WatchList;
