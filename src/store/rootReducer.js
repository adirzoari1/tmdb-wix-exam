import {combineReducers} from 'redux';

import appReducer from './app/app.reducer';
import movieReducer from './movie/movie.reducer';
import watchlistReducer from './watchlist/watchlist.reducer';

const rootReducer = combineReducers({
  app: appReducer,
  movie: movieReducer,
  watchlist: watchlistReducer,
});

export default rootReducer;
