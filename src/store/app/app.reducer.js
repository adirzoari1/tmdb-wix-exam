import Types from './app.types';

export const initialState = {
  userCountry: null,
  networkInfo: {
    isConnected: true,
    showModalVisible: false,
  },
};

const appReducer = (state = initialState, {type, payload}) => {
  switch (type) {
 
    case Types.SET_USER_COUNTRY: {
      const { userCountry} = payload
      return {
        ...state,
        userCountry,
      };
    }

    case Types.SET_NETWORK_INFO: {
      const {networkInfo} = payload;
      const showModalVisible = !networkInfo.isConnected
      return {
        ...state,
        networkInfo: {isConnected:networkInfo.isConnected, showModalVisible},
      };
    }

    case Types.CLOSE_NETWORK_MODAL: {
      return {
        ...state,
        networkInfo: {...state.networkInfo, showModalVisible: false},
      };
    }

    default:
      return state;
  }
};

export default appReducer;
