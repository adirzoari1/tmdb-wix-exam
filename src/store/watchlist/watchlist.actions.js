import Types from './watchlist.types'

const addMovieToWatchList = movie => ({
    type: Types.ADD_MOVIE_TO_WATCHLIST,
    payload: { movie}
});

const removeMovieFromWatchList = id => ({
    type: Types.REMOVE_MOVIE_FROM_WATCHLIST,
    payload: { id}
});

export {
    addMovieToWatchList,
    removeMovieFromWatchList
}