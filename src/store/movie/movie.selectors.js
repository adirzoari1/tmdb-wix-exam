import { createSelector } from 'reselect';
import { get } from 'lodash'
import { initialMoviesResponse } from './movie.reducer'
const baseMovieSelector = state => state.movie
const moviesResponseSelector = createSelector(baseMovieSelector, baseMovie => get(baseMovie, 'moviesResponse',initialMoviesResponse))
const searchMoviesResponseSelector = createSelector(baseMovieSelector, baseMovie => get(baseMovie, 'searchMoviesResponse', initialMoviesResponse))
const selectedWatchMovieInfoSelector = createSelector(baseMovieSelector, baseMovie => get(baseMovie, 'selectedWatchMovieInfo', null))
const isLoadingSelector = createSelector(baseMovieSelector, baseMovie => get(baseMovie, 'isLoading', false))

export {
    baseMovieSelector,
    moviesResponseSelector,
    searchMoviesResponseSelector,
    selectedWatchMovieInfoSelector,
    isLoadingSelector
}
