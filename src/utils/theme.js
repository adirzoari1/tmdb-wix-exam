const colors = {
  black: '#0a090d',
  black_o20: 'rgba(0,0,0,0.20)',
  black_o40: 'rgba(0,0,0,0.40)',
  purple: '#654aff',
  white: '#ffffff',
  white_o20: 'rgba(255,255,255,0.20)',
  white_o40: 'rgba(255,255,255,0.40)',
  yellow:'#f6bf47',
  grey: '#e6e6e6',
  grey_a:'#bbbfca',
};


const fonts = {
  light: 'Montserrat-Light',
  regular: 'Montserrat-Regular',
  medium: 'Montserrat-Medium',
  bold: 'Montserrat-Bold',
  semiBold: 'Montserrat-SemiBold',
  extraBold: 'Montserrat-ExtraBold',
};




export {colors, fonts};
