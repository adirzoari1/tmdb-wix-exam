import axios from 'axios';
import Config from 'react-native-config';
const API = axios.create({
  baseURL: Config.API_URL,
  params: {
    api_key: Config.API_KEY,
  },
});

export const getPopularMovies = params => API.get('/movie/popular', {params});

export const getMovieInfo = id =>
  API.get(`/movie/${id}`, {
    params: {append_to_response: 'videos'},
  });

export const searchMovies = params =>
  API.get(`/search/movie`, {
    params,
  });


