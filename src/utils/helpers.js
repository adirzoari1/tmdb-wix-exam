import {Dimensions,NativeModules} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;



const deviceLocal =
Platform.OS === 'ios'
  ? NativeModules.SettingsManager.settings.AppleLocale ||
    NativeModules.SettingsManager.settings.AppleLanguages[0] 
  : NativeModules.I18nManager.localeIdentifier;

const DEFAULT_DEVICE_LOCAL = 'IL'
const deviceLocalCode = deviceLocal.split('_')[1] || DEFAULT_DEVICE_LOCAL

const getYoutubeVideoURL = (movieItem)=> movieItem?.trailerId?`https://www.youtube.com/embed/${movieItem.trailerId}?start=0`:`https://www.youtube.com/results?search_query=${movieItem?.title}`

export {windowWidth, windowHeight,deviceLocalCode,getYoutubeVideoURL};
