/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry,I18nManager} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';

XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
    GLOBAL.originalXMLHttpRequest :
    GLOBAL.XMLHttpRequest;

  // fetch logger
global._fetch = fetch;


try { 
    I18nManager.allowRTL(false);
} 
catch (e) {
    console.log(e);
}

AppRegistry.registerComponent(appName, () => App);
